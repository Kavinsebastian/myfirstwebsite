const initialState = {
	COMMENT:'',
	NAME:''
}


const comentReducer = (state = initialState , action) => {
	switch (action.type) {
		case 'COMMENT':
			return {
				...state,
				COMMENT : action.payload}
		case 'NAME':
			return {
				...state,
				NAME : action.payload}

		default:
			return state
	}
}

export default comentReducer