import commentReducer from './commentReducer'
import { combineReducers } from "redux";

const Reducer = combineReducers({
	pesan: commentReducer,
})

export default Reducer