import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import Layout from '../components/Layout'

export default function About() {
	// const pesan = useSelector(state => state.pesan)
	const [name , setName] = useState('')
	const [comen , setComen] = useState('')
	useEffect(() => {
		const Name = JSON.parse(localStorage.getItem('name'))
		const comen = JSON.parse(localStorage.getItem('comen'))
		console.log('local',Name);
		console.log('local',comen);
		if(name == null || comen == null){
			setName('users')
			setComen('')
		}else{
			setName(Name.payload)
			setComen(comen.payload)
		}
	})

	return (
		<Layout title = 'Your Comment'>
		<div className='container h-[500px] w-auto sm:h-[600px] 2xl:h-[780px] flex sm:justify-center items-center text-center'>
			<div className="container sm:w-10/12 md:w-5/12 h-96 flex-col items-center flex text-gray-600 font-semibold">
				<div className="container w-5/12 p-5 bg-green-400 uppercase rounded-xl items-center">
					<h1>{name}</h1>
				</div>
				<div className="container bg-yellow-100 w-full mt-10 h-full border-2 border-gray-300 rounded-xl">
					<h1>{comen}</h1>
				</div>
			</div>
		</div>
		</Layout>
	)
}
