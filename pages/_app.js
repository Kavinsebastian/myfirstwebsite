import 'tailwindcss/tailwind.css'
import { applyMiddleware, createStore } from "redux";
import { Provider } from "react-redux";
import  Reducer  from "../config/redux/Reducers/indexReducers";
import thunk from 'redux-thunk';

const storeReducer = createStore(Reducer)

function MyApp({ Component, pageProps }) {
  return (
	<Provider store={storeReducer}>
		<Component {...pageProps} />
	</Provider>
		)
}

export default MyApp
