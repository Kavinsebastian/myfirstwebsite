import React from "react";
import Layout from "../components/Layout";
import { MdSettingsEthernet } from "react-icons/md";
import { AiFillSetting } from "react-icons/ai";

export default function service() {
	return (
		<Layout title='Service'>
		<div className='container h-[500px] w-auto sm:h-[600px] 2xl:h-[780px] flex sm:justify-center items-center text-center'>
			<div className="container sm:w-10/12 md:w-5/12 h-96 flex-col items-center flex text-gray-600 font-semibold text-4xl">
				<h1>Opps.... maaf ya website ini masih tahap proses Development </h1>
				<div className="container flex flex-row justify-center mt-5 md:mt-10">
				<i><AiFillSetting/></i>
				<i><MdSettingsEthernet/></i>
				</div>
			</div>
		</div>
		</Layout>
	);
}
