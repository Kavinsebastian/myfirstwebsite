import Layout from "../components/Layout";
import { useEffect } from "react";
import { Bar, PolarArea } from "react-chartjs-2";
import { BsFillPeopleFill } from "react-icons/bs";
import { FaFlag } from "react-icons/fa";
import { GiPeaceDove, GiHealing } from "react-icons/gi";
import { RiVirusLine } from "react-icons/ri";
import Card from "../components/Card";
import Comments from "../components/comments";
import Header from "../components/header";

export default function Home({
	data,
	dataDunia,
	dataSembuhGlobal,
	dataMatiGlobal,
	dataPositifGlobal,
	error
}) {

	const sembuh = data[0].sembuh.replace(/[^\d\.]/g, "");
	const meninggal = data[0].meninggal.replace(/[^\d\.]/g, "");
	const positif = data[0].positif.replace(/[^\d\.]/g, "");
	const dirawat = data[0].dirawat.replace(/[^\d\.]/g, "");
	const SembuhGlobal = dataSembuhGlobal.value.replace(/[^\d\.]/g, "");
	const PositifGlobal = dataPositifGlobal.value.replace(/[^\d\.]/g, "");
	const MatiGlobal = dataMatiGlobal.value.replace(/[^\d\.]/g, "");
	const active = dataDunia[12].attributes.Confirmed;
	const seluruh = dataDunia.length;

	useEffect(() => {
		console.log(dataDunia[12].attributes);
		console.log(data[0]);
		console.log(PositifGlobal);
		console.log(seluruh);
	});
	const dataSet = {
		labels: [
			"positif",
			"sembuh",
			"terkonfirmasi",
			"data meninggal ",
			"dirawat",
		],
		datasets: [
			{
				label: "Data COVID-19 Indonesia",
				data: [positif, sembuh, active, meninggal, dirawat],
				backgroundColor: [
					"rgba(255, 99, 132, 0.2)",
					"rgba(255, 159, 64, 0.2)",
					"rgba(255, 205, 86, 0.2)",
					"rgba(75, 192, 192, 0.2)",
					"rgba(54, 162, 235, 0.2)",
					"rgba(153, 102, 255, 0.2)",
					"rgba(201, 203, 207, 0.2)",
				],
				borderColor: [
					"rgb(255, 99, 132)",
					"rgb(255, 159, 64)",
					"rgb(255, 205, 86)",
					"rgb(75, 192, 192)",
					"rgb(54, 162, 235)",
					"rgb(153, 102, 255)",
					"rgb(201, 203, 207)",
				],
				borderWidth: 1,
			},
			{
				label: "Data COVID-19 Dunia",
				data: [PositifGlobal, SembuhGlobal, 0, MatiGlobal],
				backgroundColor: [
					"rgba(100, 99, 132, 0.2)",
					// "rgba(100, 159, 64, 0.2)",
					// "rgba(100, 205, 86, 0.2)",
					// "rgba(100, 203, 207, 0.2)",
					"rgba(75, 192, 192, 0.2)",
					"rgba(54, 162, 235, 0.2)",
					"rgba(153, 102, 255, 0.2)",
				],
				borderColor: [
					"rgb(100, 99, 132)",
					"rgb(100, 162, 235)",
					"rgb(255, 159, 64)",
					"rgb(255, 205, 86)",
					"rgb(75, 192, 192)",
					"rgb(153, 102, 255)",
					"rgb(201, 203, 207)",
				],
				borderWidth: 1,
			},
		],
	};

	const dataPolar = {
		labels: ["positif", "meninggal", "sembuh", "terkonfirmasi", "dirawat"],
		datasets: [
			{
				label: "My First Dataset",
				data: [positif, meninggal, sembuh, active, dirawat],
				backgroundColor: [
					"rgb(255, 99, 132)",
					"rgb(75, 192, 192)",
					"rgb(255, 205, 86)",
					"rgb(201, 203, 207)",
					"rgb(54, 162, 235)",
				],
			},
		],
	};

	return (
		<Layout title='Home'>

			<div className="container w-auto h-full text-center">
				<Header tagline='Data Statistics SARS CoV-19'/>
				<div className="container w-full flex-row sm:flex">
					<div className="container mt-5 pr-10 sm:ml-10">
						<Bar
							data={dataSet}
							width={'180%'}
							options={{ maintainAspectRatio: true }}
							/>
					</div>
					<div className="container mt-5 flex">
						<PolarArea width={'200%'} data={dataPolar}
							options={{ maintainAspectRatio: true }} />

					</div>
				</div>
				<div className="flex justify-center">
					<div className="container text-center shadow-lg pb-10 md:w-11/12 mt-10">
						<Header tagline='data covid-19 di seluruh dunia'/>
						<div className="flex justify-center">
							<div className="container sm:flex flex-row mt-10 h-4/6   justify-around">
								<Card
									link="#"
									judul="Total Negara"
									total={seluruh}
									icon={<FaFlag />}
									className="bg-yellow-200"
									Plink="negara"
								/>
								<Card
									link="#"
									judul="Total positif"
									total={PositifGlobal}
									icon={<RiVirusLine />}
									className="bg-green-200"
									Plink="jiwa"
								/>
								<Card
									link="#"
									judul="Total meninggal"
									total={MatiGlobal}
									icon={<GiPeaceDove />}
									className="bg-blue-200"
									Plink="jiwa"
								/>
							</div>
						</div>
					</div>
				</div>

				<div className="flex justify-center">
					<div className="container text-center shadow-lg pb-10 md:w-11/12 mt-10">
						<Header tagline='data covid-19 di indonesia'/>
						<div className="flex lg:justify-center">
							<div className="container flex-row sm:flex mt-10 h-4/6 justify-around">
								<Card
									link="#"
									judul="total dirawat"
									total={dirawat}
									icon={<GiHealing />}
									className="bg-yellow-200"
									Plink="jiwa"
								/>
								<Card
									link="#"
									judul="Total positif"
									total={positif}
									icon={<BsFillPeopleFill />}
									className="bg-green-200"
									Plink="jiwa"
								/>
								<Card
									link="#"
									judul="Total meninggal"
									total={meninggal}
									icon={<GiPeaceDove />}
									className="bg-blue-200"
									Plink="jiwa"
								/>
							</div>
						</div>
					</div>
				</div>

				<Comments />
			</div>
		</Layout>
	);
}

export const getServerSideProps = async () => {
	const [
		dataRes,
		dataResDunia,
		dataResSembuhDunia,
		dataResMatiDunia,
		dataResPositifGlobal,
	] = await Promise.all([
		fetch("https://api.kawalcorona.com/indonesia"),
		fetch("https://api.kawalcorona.com/"),
		fetch("https://api.kawalcorona.com/sembuh"),
		fetch("https://api.kawalcorona.com/meninggal"),
		fetch("https://api.kawalcorona.com/positif"),
	])

if(dataRes.status === 200){
	const [data, dataDunia, dataSembuhGlobal, dataMatiGlobal, dataPositifGlobal] =
		await Promise.all([
			dataRes.json(),
			dataResDunia.json(),
			dataResSembuhDunia.json(),
			dataResMatiDunia.json(),
			dataResPositifGlobal.json(),
		]);
	// const {data, dataDunia} = [dataRes, dataResDunia]

	return {
		props: {
			data,
			dataDunia,
			dataSembuhGlobal,
			dataMatiGlobal,
			dataPositifGlobal,
		},
	};
}else{
	const error = dataRes.status
	return{
		props:{
			error
		}
	}
}

};
