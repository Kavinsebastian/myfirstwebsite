import { useEffect } from "react";
import { useRouter } from "next/router";


export default function Custom404() {
	const router = useRouter()
	useEffect(() => {
		setTimeout(() => {
			router.push('/')
		}, 3000);
	})
	return (
		<div className='container text-center py-28 text-gray-400 font-semibold'>
			<h1 className='title-not-found'>Ooopsss .... </h1>
			<h1 className='title-not-found'>halaman yang anda cari tidak di temukan :`)</h1>
		</div>
	)
}
