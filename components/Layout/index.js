import Footer from "../footer";
import Navbar from "../Navbar";
import Head from 'next/head'

export default function Layout({title, children}) {
    return (
        <>
				<Head>
					<title>{title}</title>
					<link rel="icon" href="https://img.pikbest.com/58pic/28/63/12/16m58PICddjK6a6r33d66_PIC2018.png!wc340wm" />
				</Head>
            <Navbar/>
            {children}
						<Footer/>
            </>
    )
}
