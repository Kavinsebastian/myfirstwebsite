import React from "react";
import Link from "next/link";
export default function Card({ judul, total, link, icon, className, Plink }) {
	return (
		<div>
			<div
				className={`container flex flex-col uppercase text-center text-gray-600 items-center rounded-md shadow-lg p-10  ${className}`}
			>
				<i className="text-3xl">{icon}</i>
				<h1>{judul}</h1>
				<div className="h-3/6 justify-center flex items-center xl:text-6xl lg:text-4xl text-red-600">
					<h1>{total}</h1>
				</div>
				<div className="mt-5">
					<Link className="p-5 bg-blue-500 rounded-sm" href={link}>
						{Plink}
					</Link>
				</div>
			</div>
		</div>
	);
}
