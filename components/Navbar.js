import Link from "next/link";
import { useState } from "react";
export default function Navbar() {
	const [menuToggle, setMenuToggle] = useState(false)
  return (
    <>
      <nav className='md:bg-white shadow-lg'>
        <div className='max-w-6xl mx-auto px-4'>
          <div className='flex justify-between'>
            <div className='flex space-x-7'>
              <div>
                <a className='flex items-center py-4 px-2'>
                  <span className='font-semibold text-gray-500 text-lg'>
                    CoV-19.Info
                  </span>
                </a>
              </div>
              <div className='hidden md:flex items-center space-x-1'>
                <a className='py-4 px-2 text-gray-500 hover:text-green-500 hover:border-b-4 hover:border-green-500 font-semibold '>
                  <Link href='/'>Home</Link>
                </a>
                <a className='py-4 px-2 text-gray-500 hover:text-green-500 hover:border-b-4 hover:border-green-500 font-semibold '>
                  <Link href='/about'>Your Comments</Link>
                </a>
                <a className='py-4 px-2 text-gray-500 hover:text-green-500 hover:border-b-4 hover:border-green-500 font-semibold '>
                  <Link href='/service'>Services</Link>
                </a>
                <a className='py-4 px-2 text-gray-500 hover:text-green-500 hover:border-b-4 hover:border-green-500 font-semibold '>
                  <Link href='contact'>Contact Us </Link>
                </a>
              </div>
            </div>
							<div className="container md:hidden flex justify-end ">
								<div className="container  w-3/12 flex justify-end pb-10">
								<img src="/menu.svg" alt="" className='bg-green-500 mt-2 rounded-md' onClick={() => setMenuToggle(true)} />
								</div>
							</div>
          </div>
        </div>
      </nav>

		<div className={`fixed bg-gray-800 opacity-80 p-10 z-10 transition-all top-0 h-full md:hidden w-full ${menuToggle ? 'right-0': '-right-full'}`}>
			<div className="container flex justify-end opacity-100">
				<img src="/x.svg" alt="" onClick={() => setMenuToggle(false)} />
			</div>
			<ul className='opacity-100 text-white'>
				<li>
				<a className='py-4 px-2  hover:text-green-500 hover:border-b-4 hover:border-green-500 font-semibold '>
                  <Link href='/'>Home</Link>
                </a>
				</li>
				<li className='mt-5'>
				<a className='py-4 px-2 hover:text-green-500 hover:border-b-4 hover:border-green-500 font-semibold '>
                  <Link href='/about'>Your Comments</Link>
                </a>
				</li>
				<li className='mt-5'>
				<a className='py-4 px-2 hover:text-green-500 hover:border-b-4 hover:border-green-500 font-semibold '>
                  <Link href='/service'>Services</Link>
                </a>
				</li>
				<li className='mt-5'>
				<a className='py-4 px-2 hover:text-green-500 hover:border-b-4 hover:border-green-500 font-semibold '>
                  <Link href='contact'>Contact Us </Link>
                </a>
				</li>
			</ul>
		</div>

    </>
  );
}
