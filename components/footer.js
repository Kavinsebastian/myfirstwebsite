import React from 'react'

export default function Footer() {
	return (
		<div className='container bg-gray-200 text-center text-lg text-gray-400 font-sans h-20 w-auto mt-20'>
			<h1>made by @kavin sebastian</h1>
		</div>
	)
}
