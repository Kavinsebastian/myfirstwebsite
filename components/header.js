
export default function Header({tagline}) {
	return (
		<div className='py-5  uppercase mt-2 font-semibold'>
			<h1>{tagline}</h1>
		</div>
	)
}
