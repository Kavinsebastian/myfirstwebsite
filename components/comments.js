import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { comen, inputName } from "../config/redux/actions";
export default function Comments() {
	const pesan = useSelector((state) => state.pesan.COMMENT);
	console.log(pesan);

	const name = useSelector((state) => state.pesan.NAME)
	const [coment, setComent] = useState("");
	const [Name, setName] = useState('')
	const dispatch = useDispatch();


	const handleChangeComment = (e) => {
		setComent(e.target.value);
	};

	const handleChangeName = (e) => {
		setName(e.target.value)
	}

	const handleSubmit = () => {
			// dispatch(comen(coment))
			// dispatch(inputName(Name))
		const resComen =  dispatch(comen(coment))
		const resName = dispatch(inputName(Name))
		if(resComen){
			localStorage.setItem('name', JSON.stringify(resName))
			localStorage.setItem('comen', JSON.stringify(resComen))
		}

	}


	return (
		<div className="container w-auto pt-12 flex bg-gray-50 justify-center md:m-5">
			<div className="container mt-10 sm:w-7/12 lg:flex text-center">
				<h1>Comments:</h1>

				<div className="container h-auto flex flex-col text-sm">
					<div className="container w-10/12 ml-10 p-2 shadow-lg bg-white rounded-md ">
						<h1>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit.
							Voluptatum qui quod est rem quibusdam? Praesentium corrupti
							explicabo, doloremque animi voluptates quas deserunt obcaecati
							culpa nihil, ab dolorum iusto facilis inventore.
						</h1>
					</div>

					<div className="container rounded-lg bg-green-300  text-center w-2/12 mb-2">
						<h1>owner</h1>
					</div>
					<div className="container w-10/12 ml-10 p-2 shadow-lg bg-white rounded-md  ">
						<h1>{pesan}</h1>
					</div>

					<div className="container rounded-lg bg-green-300  text-center w-2/12 mb-2">
						<h1>{name}</h1>
					</div>
				</div>
				<div className="container sm:w-5/12 w-full text-left sm:text-right ">
					<input
						type="text"
						placeholder="masukan nama"
						onChange={handleChangeName}
						className="w-full border-2 p-2 mb-2"
					/>
					<textarea
						name="text"
						id="comen"
						cols="40"
						rows="12"
						onChange={handleChangeComment}
						className="border-gray-200 border-2 md:mb-5"
					></textarea>
					<button
						onClick={handleSubmit}
						className="hover:opacity-90 p-3 w-6/12 shadow-xl text-white bg-blue-500 rounded-sm"
					>
						submit
					</button>
				</div>
			</div>
		</div>
	);
}
